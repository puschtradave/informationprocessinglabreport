\chapter{AEP - auditory evoked potentials}

\section{Introduction}
\subparagraph*{Oddball paradigm} In the first experiment we wanted to prove the oddball paradigm, which is an experimental procedure first discovered by Nancy Squires, Kenneth Squires and Steven Hillyard\footnote{Squires NK, Squires KC, Hillyard SA. (1975) Two varieties of long-latency positive waves evoked by unpredictable auditory stimuli in man. 387-401}. Their findings are:


\begin{center}
\begin{minipage}[c]{0.7\textwidth}
\textit{
"Two distinct late-positive components of the scalp-recorded auditory evoked potential were identified which differed in their latency, scalp topography and psychological correlates. The earlier component, called "P3a" (latency about 240 msec), was elicited by infrequent, unpredictable shifts of either intensity or frequency in a train of tone pips whether the subject was ignoring (reading a book) or attending to the tones (counting). The later component, called "P3a" (mean latency about 350 msec), occurred only when the subject was actively attending to the tones; it was evoked by the infrequent, unpredictable stimulus shifts, regardless of whether the subject was counting that stimulus or the more frequently occurring stimulus. Both of these distinct psychophysiological entities have previously been refered to as the "P3" or "P300" in the literature."\footnote{https://www.ncbi.nlm.nih.gov/pubmed/46819}
}
\end{minipage}
\end{center}

\pagebreak
We were basically trying to reproduce this "P300" ERP component in form of an AEP via an auditory stimulus presentation. A participant heard a set of tones consisting of two different frequencies and had to actively focus on the higher frequency tones, which posed as the deviant ones. This is pictured in Figure~\ref{fig:one_tone_stream_001}. The higher frequency tones appeared less frequent than the lower frequency ones (oddball).

\begin{figure}[H]
	\centering
	\includegraphics[width=0.6\textwidth]{./images/one_tone_stream_001}
	\caption{Tone stream}
	\label{fig:one_tone_stream_001}
\end{figure}


\section{Methods}
The measurement was conducted by EEG, the electrodes that were analyzed were Pz, Cz and Fz (international 10-20 system).

\subparagraph*{EEG} The EEG is used to obtain any information about the inner dynamics of the brain; ideally to figure out what kind of thoughts are processed at the moment. A characteristic of EEG is that it is non-invasive. This comes with lots of benefits, but allows to measure only superpositions of individual neurons and therefore limits insight. It is known that $10^{10} - 10^{12}$ neurons are located in the human cortex. Each of them can create action potentials (spikes) of 70mV that last for approximately 1ms. The temporal and spatial summation of these spikes, which can be fired up to 1000 times per second and neuron, leads to post-synaptic potentials (PSPs) that can be inhibitory or excitatory.\\
EEG allows to measure a superposition of them. When interpreting the measurements, it is important to keep in mind that distal parts of the neo-cortex are closer to the electrodes and therefore have higher amplitudes than proximal areas.\\



The participant was sitting in front of a 15" LCD screen and was wearing a set of headphones. He was told to sit quietly and calmly to minimize artifacts, environment distractions were minimized also.

\begin{figure}[H]
	\centering
	\includegraphics[width=0.6\textwidth]{./images/bernieAEP}
	\caption{participant}
	\label{fig:bernieAEP}
\end{figure}

As an amplifier the device g$\cdot$tec (Guger Technologies) USBamp was used. To assure sufficient conductivity an impedance check was performed prior to measurement with a g$\cdot$tec Zcheck Impedance Check.

The EEG electrodes were connected to the amplifier in the order front to back, and respectively for each row left to right (see table~\ref{tab:electrodes}). 


\begin{minipage}[c]{0.45\textwidth}
\begin{figure}[H]
        \centering
        \includegraphics[width=\textwidth]{images/imped_checker.jpg}
        \caption{Impedance tester}
        \label{fig:imped_checker}
    \end{figure}
\end{minipage}\hfill
\begin{minipage}[c]{0.45\textwidth}
	\begin{figure}[H]
        \centering
        \includegraphics[width=\textwidth]{images/ampl.jpg}
        \caption{Amplifier}
        \label{fig:ampl}
    \end{figure}
\end{minipage}


\begin{table}[H]
\centering
\caption{Connection of used electrodes and amplifier channels}
\label{tab:electrodes}
\begin{tabular}{lllllllllll}
\hline
\multicolumn{1}{|l||}{Amp. Channel}   & \multicolumn{1}{l|}{Ch1} & \multicolumn{1}{l|}{Ch2} & \multicolumn{1}{l|}{Ch3} & \multicolumn{1}{l|}{Ch4} & \multicolumn{1}{l|}{Ch5} & \multicolumn{1}{l|}{Ch6} & \multicolumn{1}{l|}{Ch7} & \multicolumn{1}{l|}{Ch8} &\multicolumn{1}{l|}{GND (Y)} & \multicolumn{1}{l|}{Ref (B)} \\ \hline
\multicolumn{1}{|l||}{Electrode} & \multicolumn{1}{l|}{Fz}  & \multicolumn{1}{l|}{FCz} & \multicolumn{1}{l|}{C1}  & \multicolumn{1}{l|}{Cz}  & \multicolumn{1}{l|}{CPz} & \multicolumn{1}{l|}{CPz} & \multicolumn{1}{l|}{Pz}  & \multicolumn{1}{l|}{Oz}  &\multicolumn{1}{l|}{r. mastoid} & \multicolumn{1}{l|}{l. mastoid}\\ \hline
\end{tabular}
\end{table}


\section{Results}

The results for each considered electrode can be seen in Figure \ref{fig:Pz_oddball} to Figure \ref{fig:Fz_oddball}.


\begin{figure}
	\centering
	\includegraphics[width=0.8\textwidth]{./images/Group2_Pz_oddball.jpg}
	\caption{Pz oddball}
	\label{fig:Pz_oddball}
\end{figure}

\begin{figure}
	\centering
	\includegraphics[width=0.8\textwidth]{./images/Group2_Cz_oddball.jpg}
	\caption{Cz oddball}
	\label{fig:Cz_oddball}
\end{figure}


\begin{figure}
	\centering
	\includegraphics[width=0.8\textwidth]{./images/Group2_Fz_oddball.jpg}
	\caption{Fz oddball}
	\label{fig:Fz_oddball}
\end{figure}

\section{Discussion}
Pz, Cz and Fz show significant deviations of the lower tones (blue line) to the deviant higher tones (red line). Significant differences between the two classes were marked in green. As expected a P300 potential (Figures~\ref{fig:Pz_oddball},~\ref{fig:Cz_oddball} and~\ref{fig:Fz_oddball}) can be observed. It appears at around 350-400ms after stimulus onset. We can therefore confirm the findings of the oddball paradigm.
